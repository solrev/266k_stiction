function [ ] = Flake_Model_1_0(  )
%% HEADER 
% Flake Model 
% Author: Greg Kline
% Revision: 1.0
% Date: 11/26/2015
% 
% This model is a continuation and specification of a previous model.  Here 
% the focus will be on the combined PTFE lubricant and olid particles of a
% flake nature.  Recent evidence shows the general shape of WS2 particles
% to be hexagonal flakes.  The same geometric approach to the Van der Waals
% interactions will be used.  
%
% Unlike the previous model, the surface roughness is not considered as the
% assumption is the lube channel width is much greater than the surface
% roughness
% 
% Considerations will be made for surface electromagnetic interations.

%% INPUTS 

% Block Geometry 
% Width (m)
Block_width = .050; % Orthogonal to motion

% Length (m)
Block_length = .050; % in motion

% Mass (kg)
Block_mass = .669; 

% Lube thickness (m)
Lube_thickness = .0007; 

% Solid Lube Type 
solid_lube = 'WS2';

% Ratio of liquid to solid 
Lube_ratio = 10; % 10:1 liquid to solid 

% Define specific parameters
switch solid_lube
    % Tungsten Disulfide
    case {'WS2'}
        % Shape geometry 
        % Density WS2 (kg / m^3 ) 
        Density_solid = 7500; 
        
        % Define the shape parameter
        solid_shape = 'Flake';
        
        % Flake length (m)
        Flake_length_RMS = 1e-6; 
        
        % Flake Height (m)
        % From the SEM images, it can be seen that even if flake size 
        % changes, the flake thickness remains relatively constant.  This 
        % thickness is based on micrometer measurements of SEM image at 
        % 1um
        Flake_height = 20e-9;  
    
    otherwise
        Lube_error = sprintf(['Your lubricant is not contained in this' ...
            ' model.  You may open and alter the code to add this. \n']);
        display(Lube_error);
end
        
%% CONSTANTS 

% Gravity (m/s^2)
Gravity = 9.8066; 

% Block weight (N)
Block_weight = Block_mass * Gravity;

% Atomic Geomtery
% Van der Waals bond length of PTFE (m)
VdW_bond_length = 135e-12;

% Flake to Flpourine bond width (m)
Flake_Flourine_bond = VdW_bond_length / 2; % just an assumption for now 

% Flourine radius (m)
Radius_Flourine = 65e-12; % covelant radius 

% Flourine mass per atom (kg)
Mass_Flourine = 18.998403163 / 1000 / 6.022e23; %atomic mass in g/mol -> kg

% Carbon radius (m)
Radius_Carbon = 69e-12; % covelant radius

% Carbon mass (kg)
Mass_Carbon = 12.011 / 1000 / 6.022e23; %atomic mass in g/mol -> kg

%%%%%%%%%%%%%%%%%%%%%%%%%%
% PTFE physical quantities
% PTFE density (kg/m^3)
PTFE_density = 888.9356935;

% PTFE em
em = 2.1;

% PTFE contact angle (deg)
theta_c = 95 * pi / 180; %deg

% PTFE surface SOMETHING (N/m)
y_la = 63e-3; 

% PTFE type 
vis_type = 'bingham';

% PTFE shear threshold (Pa)
t0 = 311.2301746; % %  713.2240979 * (1 - 1/ls); %Pa
                
% PTFE Hamaker Constant
% A = C*pi^2*n*n
%A_lube = 3.8e-20; % Hamaker for PTFE
%n_lube = 5.3523691e27; % number density PTFE
% Molecular Interaction constant [Israelachvili]
C_lube = 1.3453404e-76; 
                
% Unit block geometry 
% This is the standard unit for a PTFE based lubricant and in the geometric
% basis for the distribution of lubricant
% /  F   F  \^n
% |  |   |  |
% |  C - C  | 
% |  |   |  |
% \  F   F  /
%
% Unit width (m)
Unit_width = 2 * Radius_Flourine; 

% Unit height (m)
Unit_height = Radius_Flourine;

% Unit Length (m)
Unit_length = 2 * Radius_Flourine + Radius_Carbon;

% Unit Mass (kg)
Unit_mass = 4 * Mass_Flourine + 2 * Mass_Carbon; 

% Volume of Lube (m^3)
Volume_lube = Block_width * Block_length * Lube_thickness; 

% Mass of lube (kg) 
Mass_PTFE_lube = PTFE_density * Volume_lube;

% The lubricant will be "formed" into even sheets of rectangular prisms
% representing the PTFE base units.  The sheet is built, then based of
% specific conditions, the total number of sheets can be found.  In the
% model, the inter-sheet attractive force will be found and the number of
% sheets will determine the total force.
% Number of units (#)
Number_lube_units = Mass_PTFE_lube / Unit_mass;

% Number of units wide (#)
Number_units_wide = Block_width / Unit_width;

% Number of units tall (#) 
Number_units_high = Lube_thickness / Unit_height;

% Number of units per sheet (#)
Number_per_sheet = Number_units_wide * Number_units_high;

% Number of sheets (#)
Number_of_sheets = Number_lube_units / Number_per_sheet;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% MODEL

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distribution of flakes 
% Make a distribution for flake lengths 
distribution = 'Normal';

% Find the mass of the lube based off of the ration of solid to liquid (m)
Mass_solid_lube = Mass_PTFE_lube / Lube_ratio;

% Establish the standard deviation  [for now 50nm ] (m)
sig = 5e-8;

% Set the mean for the function (m)
mu = Flake_length_RMS;

% Find the area points to analyze. Begin by finding 101 points evenly 
% spaced under the standard normal curve using 101 points will give 100 
% regions in the next part (points)
CDF_x_points = linspace(-6,6,101);

switch distribution
    case 'None'
        % The non distributed version is basically a Dirac distribution
        Percent_area_under_CDF = 1; 
        
    case 'Normal'
        % Next build a loop that subtracts the CDF value between two 
        % points.  Since this is standard normal curve, the value 
        % found is the area under CDF in that space is directly the
        % percentage of the Normal PDF this region occupies (%area) since 
        % the area under a PDF = 1 and well over 99% of it exists in 
        % first six sigma
        for i = 1:length(CDF_x_points) - 1
            Percent_area_under_CDF(i) = normcdf(CDF_x_points(i+1)) - ...
                normcdf(CDF_x_points(i));
        end
    
    case 'Weibull'
        % The Weibull distribution is better represnetative of the results
        % of crushing and milling operations as would occur in creation of 
        % WS2 and such
        for i = 1:length(CDF_x_points) - 1
            Percent_area_under_CDF(i) = wblcdf(CDF_x_points(i+1), 1.5, 2) ...
                - wblcdf(CDF_x_points(i), 1.5, 2);
        end   
        
    otherwise 
        display(' Error: distribuiton of flake length error' );
end
        
% Now a vector representing the percentage of area 
% occupied, exists find the %mass of each unit ( % mass )
Percent_mass_per_CDF_unit = Mass_solid_lube * Percent_area_under_CDF;

% Hexagon volume constant
Volume_constant = 3 * sqrt(3) / 2;
    
% Now that the distribution relative area have been figured, the physical
% properties can be calculated:

% First build a vector representing the dstributed lengths
Length_distribution = linspace( mu - 6 * sig, mu + 6 * sig, ...
    length(CDF_x_points));

% Average adjacent lengths to give an approximation of the length
% that accompanies the %mass (m)
if ~strcmp(distribution, 'None')
    Flake_length_distribution = mean([Length_distribution(1:end-1); ...
        Length_distribution(2:end)])
else 
    Flake_length_distribution = Flake_length_RMS;
end

% Now build the hexagon geometry
% First find the base (m) 
Flake_base_distribution = Flake_length_distribution ./ 2;

% Next is the volume (m^3)
Flake_volume_distribution = Volume_constant * ...
    Flake_base_distribution.^2 * Flake_height;

% Now find the mass of these volumes (kg)
Flake_mass_distribution = Flake_volume_distribution * ...
    Density_solid;

% Now that the mass each unit of our distribution occupies is found,
% and the mass of a flake in each of these regions is known, find the
% number of flakes per region by just dividing the masses (#)
Number_flakes_per_CDF_region = round ( Percent_mass_per_CDF_unit ./ ...
    Flake_mass_distribution );

% Now that the total number of flakes of each size is known, divide by the 
% number of PTFE sheets to distribute these flakes "evenly" (#)
Number_flakes_per_sheet_per_size = Number_flakes_per_CDF_region ./ ...
    Number_of_sheets;

% Next, the number of flakes it takes to occupy a vertical column of the 
% current lube thickness needs to be found.  The width of a hexagon is
% sqrt(3) * the base (#)
Number_flakes_per_column_per_size = round ( Lube_thickness ./ ( sqrt(3) ...
    * Flake_base_distribution )); 

% Find the total number of columns per size by dividing total per sheet by
% total per size (#)
Number_flake_columns_per_size = round (Number_flakes_per_sheet_per_size ...
    ./ Number_flakes_per_column_per_size );

% Force accross one flake of a particular size (N) 
F_per_flake_per_size = ( 15 * pi * C_lube * Flake_length_distribution ) / ...
    ( 8 * Radius_Flourine^2 * Flake_height^6 );

% Find the total force per column per size of each flake (N)
F_per_column_per_size = Number_flakes_per_column_per_size .* ...
    F_per_flake_per_size;

% Find the total force per each flake size by multiplying by number of
% columns per sheet (N)
F_per_flake_size = F_per_column_per_size .* Number_flake_columns_per_size;

% Find the total force of the flakes in on sheet by summing up the column
% forces (N)
F_total_flake = sum(F_per_flake_size);

% Force between atoms at the boudary transition between flakes and VdW (N)
% Initially, the assumption will be made that a 45 degree angle of Flourine
% atoms creates the transition from atoms at the flake width to the atoms 
% at the Van der Waals width
% Find the maximum width of the atoms and flake (m)
Width_flake_and_bond = Flake_height / 2 + Flake_Flourine_bond;  

% Find the lower width value (m)
Width_bond_minimum = VdW_bond_length / 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Boundry conditions 
% Total height of the boundary condition (m)
Boundry_height = Width_flake_and_bond  - Width_bond_minimum; 

% Develop the boundry condition [linear]
% Boundry angle (deg to rad) 
theta_boundry = 45 * pi / 180; 

% Boundry length (m)
Boundry_length = Boundry_height / tan(theta_boundry);

% Find the force between each of the unit blocks along the boundry (N)
for i = 1:round(Boundry_length / Unit_width)
    % Find this loops y value by trig 
    y(i) = Unit_width * i * tan(theta_boundry);
    
    % since this is developed over a triangle, to find the sheet force we 
    % need to double the y length to establish the radius
    % Force this loop (N)
    F_boundry_flake_loop(i) = C_lube / (2 * y(i))^7;
    
end

% Find the total force from a single flake boundry side (N)
F_boundry_per_flake_side = sum(F_boundry_flake_loop);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Non disturbed atoms 
% Find the length of atoms at VdW length. The length is the block
% width minus the widh occupied by flakes and minus the length of the
% boundrys of these flakes (m)
l_VdW = Block_width - sum (Number_flake_columns_per_size .* ...
    Flake_length_distribution) - sum(Number_flake_columns_per_size) * ...
    Boundry_length;

% Force between atoms at VdW length (N)
% W(r) = -3 * pi * C_lube * L / ( 8 * r_Fl^2 * r^5 ) 
% F(r) = dW/dr => F(r) = 15 * pi * C_lube * L / (8 * f_Fl^2 * r^6)
F_VdW_per_sheet_single_thickness = ( 15 * pi * C_lube * l_VdW ) / ...
    ( 8 * Radius_Flourine^2 * ( VdW_bond_length * 2)^6 );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find total forces 

% Total force of one sheet (N)
% Fisrt account for the force from the flake interactions.  Second take the
% force frome one side's boundry layer and multiply by the total number of
% flakes per sheet.  Finally, to find how many layers of atoms at the Van
% der Waals length, te average of how many flakes per column is used.  
F_per_sheet = F_total_flake + 2 * F_boundry_per_flake_side * ...
    sum(Number_flakes_per_sheet_per_size) + ...
    F_VdW_per_sheet_single_thickness * ...
    mean(Number_flakes_per_column_per_size);

% Correction (N)
switch distribution
    case 'None'
        geo_correction = 3.165e-5;
    case 'Normal'
        geo_correction = 3.065e-5;
    case 'Weibull'
        geo_correction = 3.064e-5; %when using 1, 3022.642304
    otherwise 
        display('There is a distribution error in the geo correction');
end

F_shear = F_per_sheet * geo_correction;

% Find the PTFE related values
%% OUTPUT 

display(sprintf(' The block weight is %d N ', Block_weight));
display(sprintf(' The shear force calculated is %d N ', F_shear));
display(sprintf(' The calculated stiction coefficient is: %1.6f ', F_shear / Block_weight)); 
end

