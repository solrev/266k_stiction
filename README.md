# README #

This file is a standalone Matlab .m file.  It should be placed in whatever directory you plan to use for analysis.  The help information is contained in the first cell.  Simply typing 'help simple_friction' will bring up all the help information.

### What is this repository for? ###

This repo will contain all the files for a NASA sponsored senior design project.  


### Contribution guidelines ###
Feel free to alter and upload at will.  Any merge issues should be addressed with Greg Kline prior to finalization.  

### Who do I talk to? ###
Any questions or concerns can be forwarded to Greg Kline: rwing27@hotmail.com with the subject
"266 NASA bits"