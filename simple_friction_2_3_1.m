 
%% HEADER DOCUMENTATION %%
% Simple Stiction Calculator
% Author: Greg Kline
% Date: 24 Nov 2014
% Revision: 2.3.1
% 
% "Essentially, all models are wrong, but some are useful." -Box
% 
% This code is to provide a simple friction calculator
% for a combination of metals and lubricants and takes 
% into account the following:
%     surface roughness condition (DMT)
%     Casimir Effect (simplified)
%     Lubricant surface tension
%     Lubricant Shear
%     Solid Shear 
%     Normal Weight Forces 
%     Temperature and solids Lubricant quantity
%
% All measurements will be in the MKS system (SI)
% The Code uses Matlab's Code Folding, so expand as needed
%
% Surface roughness
%     F = -2*A*y*cos(theta)/g (negative to denote attraction)
%     A - area of contact
%     y - surface tension (liquid to air)
%     theta(contact angle)
%     g - gravity
%     F - Force needed to balance the pressure caused by lube
% 
% Casimir Assumptions
%     For this model we assume:
%         magnetic permittivity is negligible
%         permittivity ratios are not dependent on frequency (removes integral)
%         finite speed of light (dependent on em and l)
%         integral approximated as large sum (near infinite)
%     The pressure from Casimir will be worked into the normal pressure for the 
%         friction model.
%     Seperation_Distance - Distance between the plates (m)
%     Length_along_Motion - Distance of area in the direction of motion (m)
%     Length_ortho_motion - Distance of area orthogonal to the motion (m)
%     Temp - Temperature of the interaction (K)
%     metalA - The metal on the left or bottom
%     metalB - The metal on the right or the top
%     Lube - Type of lubricant used
%     freq_range - Maximum number of Xi frequencies to add to 
%         pressure equation (#)
%     s - starting sigma to add (3.25 (1), 5.75(6))
% 
% Contact Model
%     For this portion, the disparities are modelled first as spheres whose 
%     radii are distributed according to the distrabution in the code. 
%     (normal is default). The contact area is calculated using adhesive 
%     DMT theory.  From this, the disparity then considered a cantilever 
%     beam with cross-sectional area of the contact area. The force necessary
%     to break this beam is then found.  Using the distrabution and total 
%     area, the total number of beams in each standard deviation can be 
%     found. The total force to break these beams can be found by multiplying 
%     their respective breaking forces times thenumber per deviation.  
%     These are summed and a factor is added to accountfor the fact that
%     not every beam in each deviation being broken. This factor is modelled
%     as a log normal curve with weights being on the larger sigma values.
%     (ex .5% of all 5th deviation break).
%     
% Lube Model
%     For plate distances less than the 6th sigma beam radius, the 
%     appropriate beam breaking force is added.  Then the shear force 
%     is added based on fluid static shear coefficient and plate area 
%     If solids are added, the surface area of the solids is removed 
%     from the total area and the shear is recalculated.  This sis 
%     accomplished by fitting a curve between the super lube with and 
%     without solids.  In addition, the change in temperature is calculated
%     using the Krytox information. The lubricants used here are either 
%     PTFE or PFPE in nature.
%     
% Solid Model
%     The solid model relies on the contact angle between spheres.  this 
%     creates a normal force that is used to find the DMTY contact area.  
%     the shear force is then orthogonal to the sphere.In addition, 
%     a component of the downward force counters the shear force and must
%     be accounted for.  For future work a rolling force must be taken 
%     into account. Currently only sliding is available and this results 
%     in about an order of magnitude high
% 
% Pseudo Code
%     Help File  
%     Input Function
%     Variables
%         Constants
%         Inputs
%             Each input is prompted
%             if (~prompt)
%                 set default
%             end
%         Radii Distribution calculation
%             Sigmas to be considered
%             Actual radii given RMS
%             Correction factors
%         Switch metal A
%             Assign metal A props
%             Default Al
%         Switch metal B
%             Assign metal B props
%             Default Al
%         Switch Lube
%             Assign Lube Props
%             Default Vacuum
%         Shear model type
%             Use lube and plate distance to find model
%                 Contact only
%                 Lube w contact 
%                 Lube only
%                 Solid w contact 
%                 Solid only
%     Functions
%         Find Austin TX gravity
%         Casimir Functions
%             Eigen frequencies
%             del_bar for electrical permittivity
%             finite speed of light factor
%         Contact model
%             equivalent Young's Modulus
%             equivalent radii
%         Electrical Permittivity (Drude Model)
%     Main Loop
%         Casimir Functions
%             Casimir Pressure
%                 find 0th term
%                 for i:max Eigen number
%                     find finite factor
%                     find permittivity 
%                     add ith pressure term to total pressure
%                 end
%             Casimir force 
%                 Pressure * Area
%         Lubricant Normal
%             surface tension * A / g
%         Weight normal
%             mass * gravity
%         Total Normal
%         Shear Contact Model
%             Find Young's
%             Find the percentage of area under PDF for each half sigma 
%             Divide total normal force amongst each area
%             Multiply each percentage by total area then divide out pi leaving %ri^2
%             Find total number 
%                 total ri^2/ri^2
%             Find effective contact radius
%             Find DMT contact area
%             Find effective radius
%             Find beam moment of inertia
%             Find breaking force
%             Correct the vector for NaN's since indexing was used to reduce number of 
%                 loops 
%             Find total breaking force per sigma and correct for % breaking
%             Find total force per material
%             Total shear is minimum of A vs B
%         Fluid Model
%             Area * shear threshold
%             add contact model if needed
%         Solid Model
%             %establish angles 
%             %ADD A DISTRABUTION TO ACCOUNT FOR HOW MANY OF EACH ANGLE
%             %total number of contact grains 
% 
%             for 
%                 %find the area under a normal curve using 0rad as -6sig and pi/2 
%                 %rad as 6sig hence near pi/4 is majority of prob mass 
%             end
%             %split the total normal force evenly across each grain
%             Fg = abs(F / n_g);
%             %find the normal contact force between two grains 
%             Fn = Fg * sin(theta_g);
%             %find the contact radius using DMT theory
%             a_g = real((3 * gs .* (Fn + 4*y_la*pi*gs) ./ (4 * Eg)).^(1/3));
%             %contact pressure (assume constant)
%             p0 = Fn / a_g;
%             %t_i = 3; %just a marker for now
%             %find the individual shear force  
%             %Tsi = Fn .* cos(theta_g) + .2 * Fn; %t_i .* a_g;
%             %tsi  = opposing normal component + 12p0^5*R^3/(l^3*E^4)  from Goryacheva
%             Tsi = Fn .* cos(theta_g) + 12 .* p0.^5 .* gs.^3 / ((2*gs)^3 .* Eg^4);
%             %find the total shear force for each contact angle
%             Ts_n = Tsi .* C_angle;  
%             %Sum each grain's contribution to toal force
%             Ts = sum(Ts_n);          
%         Shear Model
%             switch contact type
%                 Contact only
%                     Fs = contact only
%                 Lube w contact
%                     Fs = contact and fluid shear
%                 Lube only
%                     Fs = fluid shear only
%                 Solid w contact 
%                     Fs = contact and solid shear
%                 Solid only
%                     Fs = solid shear only
%             end
%         Outputs
%             Individual Factors
%             Total Shear 
%        
%  Any issues questions or concerns can be addressed to Greg Kline: rwing27@me.com Please 
%  use the subject: NASA Stiction Model Issues in the subject line.  This code is free to 
%  distribute and use, with modifications, given author representation.
%  
%     function [ ] = simple_friction( metalA, metalB, Lube)

%%
function [ ] = simple_friction( metalA, metalB, Lube)
display(sprintf('\n')); %make a new line
%% VARIABLES AND CONSTANTS %%
%Constants
k = 1.3806488e-23; %Boltzmann constant (m^2-kg / s^2-K)
h_bar = 1.05457172647e-34; %Reduced Plank Constant (J-s)
c = 299792458; %speed of light (m/s)
Psum_l = 0; %place holder for the summation portion of Casimir Pressure Eqn (Pa
K = 2; %contact mechanics value for true contact area
y = 0; %surface energy

%VARIABLE ASSIGNMENT
%distance of area in the direction of motion (m)
w = input(sprintf(['What is the plate dimension in direction of motion? (m) \n'...
    ' hit [enter] for default of 0.05099m\n'])); 
if isempty(w) 
    %experimental data
    w = 0.05099;   
end

%distance of area orthogonal to the motion (m)
h = input(sprintf(['What is the plate dimension in direction orthogonal to motion?'...
    '(m) \n hit [enter] for default of 0.05265m\n'])); 
if isempty(h)
    %experimental data
    h = 0.05265;   
end

%Mass of the block (kg)
m = input(sprintf(['What is the mass of the block (kg)?'...
    '\n hit [enter] for default of .66901kg\n'])); 
if isempty(m) 
    %mass of experimental aluminum block and test rig assy
    m = 0.66901;   
end

%Ambient temperature (K)
T = input(sprintf(['What is the ambient temperature (K)?'...
    ' \n hit [enter] for default of 300K\n'])); 
if isempty(T) 
    T = 300;   
end

%Maximum number of Xi frequencies to add for casimir
nMAX = input(sprintf(['What is the number of Eigen Frequencies you''d like to use?'...
    ' \n hit [enter] for default of 10,000\n'])); 
if isempty(nMAX) 
    nMAX = 10000;   
end
%Maximum number of Xi frequencies to add for casimir

%root means square of surface slope of the material A
h_pri_A = input(sprintf(['What is RMS roughness of top plate (m)?'...
    ' \n hit [enter] for default of 7.976e-8m\n'])); 
if isempty(h_pri_A) 
    h_pri_A = 7.976e-08;   
end

%root means square of surface slope of the material B
h_pri_B = input(sprintf(['What is RMS roughness of top plate (m)?'...
    ' \n hit [enter] for default of 9.825e-7m\n'])); 
if isempty(h_pri_B) 
    h_pri_B = 9.825e-7;   
end

%Distance between the plates (m)
l = input(sprintf(['What is the plate seperation distance? (m) \n'...
    ' hit [enter] for default of combined RMS\n'])); 
if isempty(l) 
    l = h_pri_A + h_pri_B;   
end

At = w * h;  %total area of block (m^2)
sigA = .8; %used for the Radii PDF sigma. 
sigB = .8;

%check if plate distance (ie lube layer) is less than the rms total values
%if so, then the peaks will make contact and we can use the cantilever
%beam theory.  otherwise we have to use a liquid shear or rolling friction
%model
%define threshold length
l_thres = 5.75 * h_pri_A * sigA + 5.75 * h_pri_B * sigB;
if l <= (l_thres)
    contact_model = true;
elseif l > (l_thres)
    contact_model = false;
else 
    display('Your plate distances are way off');
end

%define radii size for each sigma
%set sigma mean radii
s_R = [.25 .75 1.25 1.75 2.25 2.75 3.25 3.75 4.25 4.75 5.25 5.75 ];

%find radii for each block
s_RA = s_R * h_pri_A * sigA;
s_RB = s_R * h_pri_B * sigB;

%sum them up to see the total touching length
s_RT = s_RA + s_RB;

%find the combined radii larger than the plate distance as these are the
%ones making contact
s = find(s_RT >= l);

%define breaking coefficients as the percentage of beams in that region
%that break
%C = [.00 .00 .00 .00 .00 .00 .00 .0002 .02 .15 .2 .3 ];
%use log normal to define break away factors
C = lognpdf(s_R,0,.0925);
% flip so weights are heavy towards larger sigma values
C = fliplr(C);

%define material properties
%matweb.com
%e metal properties made up for now (9-28)
%temperature dependence of VdW from experiments
t_temp = @(t0) t0 * (4.24214e-3 * T - 1.12655e-2);

switch metalA
    case {'6061-T6','6061','Al','Aluminium'}
        eA = .5;
        rho = 2700; %kg/m^3
        EA = 69e9; %Young's Modulus (Pa)
        vA = .35; %Poisson's Ratio
        SA = 207e6; %Shear Strength (Pa) 
        display('Aluminium Top')
    case {'17-4SS','SS','Stainless'}
        eA = .6;
        rho = 7800; %kg/m^3
        EA = 200e9; %Young's Modulus (Pa)
        vA = .272; %Poisson's Ratio (ESTIMATED FROM 18-8)
        SA = .577 * 1310e6; %Shear Strength (Pa)
        display('Stainless Top')
    case 'Brass'
        eA = .7;
        rho = 8490; %kg/m^3
        EA = 97e9; %Young's Modulus (Pa)
        vA = .311; %Poisson's (ESTIMATED FROM 70-30)
        SA = 235e6; %Shear Strength (Pa)
        display('Brass Top')
    otherwise
        eA = .5;
        rho = 2700; %kg/m^3
        EA = 69e9; %Young's Modulus (Pa)
        vA = .35; %Poisson's Ratio
        SA = 207e6; %Shear Strength (Pa)
        display('Material Unknown, Please Try Again')
end

switch metalB
    case {'6061-T6','6061','Al','Aluminium'}
        eB = .5;
        rho = 2700; %kg/m^3
        EB = 69e9; %Young's Modulus (Pa)
        vB = .35; %Poisson's Ratio
        SB = 207e6; %Shear Strength (Pa)
        display('Aluminum Bottom')
    case {'17-4SS','SS','Stainless'}
        eB = .6;
        rho = 7800; %kg/m^3
        EB = 200e9; %Young's Modulus (Pa)
        vB = .272; %Poisson's Ratio (ESTIMATED FROM 18-8)
        SB = .577 * 1310e6; %Shear Strength (Pa)
        display('Stainless Bottom')
    case 'Brass'
        eB = .7;
        rho = 8490; %kg/m^3
        EB = 97e9; %Young's Modulus (Pa)
        vB = .311; %Poisson's (ESTIMATED FROM 70-30)
        SB = 235e6; %Shear Strength (Pa)
        display('Brass Bottom')
    otherwise
        eB = .5;
        rho = 2700; %kg/m^3
        EB = 69e9; %Young's Modulus (Pa)
        vB = .35; %Poisson's Ratio
        SB = 207e6; %Shear Strength (Pa)
        display('Material Unknown, Please Try Again')
end

switch Lube
    case {'Vacuum','Space'}
        %electrical permittivity
        em = 1;
        %surface tension angle
        theta_c = 0;
        %surface free energy
        y_la = 0; 
        %lubricant broad type
        lube_type = 'dry';
        %for display only
        display('Vacuum: No Lubricant')
        ts = 0; %static shear
    case {'WS2','Tungston-Silfide','Tungston-Disulfide','Tungston'}
        em = 4;
        theta_c = 0; %no surface tension (solid)
        y_la = 0; 
        lube_type = 'solid';
        %YOuing's for WS2 (ASSUMED)
        Eg = 411e9; 
        %make sure to set the grain size 
        gs = input(sprintf(['I see you are using WS2 lubricant. \n What is' ...
            'the grain size you are using? (m) hit [enter] for default of 20e-9m\n']));
        if isempty(gs)
            gs = 20e-9; %m
        elseif gs < min(h_pri_A,h_pri_B)
            %reduce RMS radius by WS2 grain size.
            h_pri_A = h_pri_A - gs;
            h_pri_B = h_pri_B - gs;     
        elseif gs >= min(h_pri_A,h_pri_B)
            %rolling friction model
            contact_model = false;
        else
            display('Error in WS2 Grain Size');
        end  
        display('Tungsten-Disulfide Lubricant')
    case {'Combined','Solid and Liquid','Liquid and Solid','Both'}
        %ask for the particulars of the lube
        lb = input(sprintf(['I see you are using a solid and liquid combination.\n'...
            'What liquid are you using? hit [enter] for default (Super Lube).\n']));
        if isempty(lb)
            lb = 'Super Lube';
        end
        
        ls = input(sprintf(['Also, what is the liquid to solid ratio (decimal form' ...
            'ex 10 for 10:1)? hit [enter] for default of 10:1\n']));
        if isempty(ls)
            ls = 10;
        end
        
        t_solids = @(t0) (t0 * -1.8908 + .2635); %from experimental WS2_SL
        % OLD: area version was t0 * (1 - 1/ls)
        %assign normal liquid properties, but adjust threshold shear
        %to account for reduction in shear area due to dispersed solids
        switch lb
            case {'WhLi','White Lithium','Lithium'}
                em = 3;
                theta_c = 45 * pi / 180; %rad
                y_la = 82.55e-3; %(N/m)
                %maintin as a liquid model for now
                lube_type = 'liquid';
                %liquid shear type: newtonian, bingham, maxwell, etc ... 
                vis_type = 'bingham';
                %threshold shear stress
                t0 = t_solids(587.4510607); % %Pa
                display('Whiite Lithium Lubricant')
            case {'Super','Super Lube'}
                em = 2.1;
                theta_c = 95 * pi / 180; %deg
                y_la = 63e-3; %(N/m)
                lube_type = 'liquid';
                vis_type = 'bingham';
                t0 = t_solids(311.2301746); % %  713.2240979 * (1 - 1/ls); %Pa
                display('Super Lube!')
            case {'Krytox','Big Money Lube'}
                em = 2.1;
                theta_c = 95 * pi / 180; %deg
                y_la = 63e-3; %(N/m)
                lube_type = 'liquid';
                vis_type = 'bingham';
                t0 = t_solids(408.6837464); %  %566.8870912 ; %Pa
                display('Krytox!')
        end 
    case {'Experimental Combination', 'Real SL_WS2','SLWS2'}
        em = 2.1;
        theta_c = 95 * pi / 180; %deg
        y_la = 63e-3; %(N/m)
        lube_type = 'liquid';
        vis_type = 'bingham';
        t0 = t_temp(100.8445351);  %311.2301746;%(exp 2) % (exp1)713.2240979; %Pa
        display('Super Lube!')        
    case {'WhLi','White Lithium','Lithium'}
        em = 3;
        theta_c = 45 * pi / 180; %rad
        y_la = 82.55e-3; %(N/m)
        lube_type = 'liquid';
        %liquid shear type: newtonian, bingham, maxwell, etc ... 
        vis_type = 'bingham';
        %threshold shear stress
        t0 = t_temp(587.4510607); %Pa
        display('White Lithium Lubricant')
    case {'Super','Super Lube'}
        em = 2.1;
        theta_c = 95 * pi / 180; %deg
        y_la = 63e-3; %(N/m)
        lube_type = 'liquid';
        vis_type = 'bingham';
        t0 = t_temp(311.2301746);%(exp 2) % (exp1)713.2240979; %Pa
        display('Super Lube!')
    case {'Krytox','Big Money Lube'}
        em = 2.1;
        theta_c = 95 * pi / 180; %deg
        y_la = 63e-3; %(N/m)
        lube_type = 'liquid';
        vis_type = 'bingham';
        t0 = t_temp(408.6837464); %566.8870912; %Pa
        display('Krytox!')
    case {'Air','None','NA','Dry'}
        em = 1.00058986;
        theta_c = 0; %no surface tension
        y_la = 0; 
        lube_type = 'dry';
        ts = 0;
        display('Air: No Lubricant')
    case {'Firefly','firefly','browncoat'}
        em = 1;
        theta_c = 0; %no surface tension
        y_la = 0;
        lube_type = 'dry';
        display(sprintf(['I don''t have time to take you on the wonderful \n' ...
        'journey that is this very short-lived show.  It is the \n' ...
        'ultimate combination of comedy, action, sci-fi combined \n' ...
        'with stunning performances from all the cast members. \n' ...
        'No show has ever or will ever bring to light the perfect future \n' ...
        'that the Whedonverse provided us.  It was half a season of delight \n' ...
        'followed by a lifetime of sadness.  So just keep flying. \n' ...
        'Shiny!']));
    otherwise
        em = 1;
        theta_c = 0; %no surface tension
        y_la = 0;
        lube_type = 'dry';
        display('Material Unknown, Please Try Again')
end
 
%CHOOSE THE SHEAR MODEL DESTINATION 
%based on the inputs there are 5 possibilities:
% Dry friction
% Lube layer that is less than 6 sigma so dry still applies
% Lube that is thick enough to be pure shear
% WS2 or solid that is so small it only fills larger peaks
% WS@ or solid large enough to build a pure rolling style model

if (contact_model && strcmp(lube_type,'dry'))
    shear_model = 'Contact only'
elseif (contact_model && strcmp(lube_type,'liquid'))
    shear_model = 'Liquid with contact'
elseif (~contact_model && strcmp(lube_type,'liquid'))  
    shear_model = 'Liquid Only'
elseif (contact_model && strcmp(lube_type,'solid'))
    shear_model = 'Solid with contact'
elseif (~contact_model && strcmp(lube_type,'solid'))
    shear_model = 'Solid only'
else
    display('Shear model selection error');
end

shear_model
%% FUNCTION DEFINITIONS %%

%gravity assumption for Austin (30* 16' 0"N / 97* 44' 34"W)
% 30* 16' ~ 30.266666*
g = 9.780327 * (1 + .0053024 * sin(30.266666)^2 - 5.8e-6 * sin( 2 * 30.266666)^2); %(m/s)

%CASIMIR RELATED FUNCTIONS
%find the discrete frequency n for the current photon energy level
%at the temperature condition kT
%discrete-positive frequency
% E_n = 2(pi)kTn/h_bar
zai_n = @(T,n) 2 * pi * k * T * n / h_bar;

%dielectric response (SIMPLIFIED)
%this is the function for dielectric response of the material and the 
%medium in the center.  It is the simplified version as the full version
%takes into account surface conditions as well. This will be presented in a
%more complex model.
% del = (ei - em) / (ei + em)
del_bar = @(ei,em) (ei - em)/(ei + em);

%travel time relative to fluctuation lifetime
%this simplified form takes into account the finite speed of light and the
%fact that the objects are separated by a distance l so molecules in metal A
%disturb ones in B but not at the exact moment
% rn(l) = 2sqrt(em)E_nl/c
rn = @(em,l,T,n) 2 * sqrt(em) * zai_n(T,n) * l / c;

%CONTACT MECHANICS FUNCTIONS
%equivalent young's modulus
E_star = @(E1,E2,v1,v2) ( (1-v1^2)/E1 + (1-v2^2)/E2 )^-1;

%equivilent radius
R_star = @(R1,R2) ( 1./R1 + 1./R2 ).^-1;

%find the electrical permittivity using Drude model
function [e] = e_Permittivity(metal, w)
    %w is the eigen frequency 
    switch metal
        case {'6061-T6','6061','Al','Aluminium'}
            %find electron density
            %n = NA * Z(atomic #) * rho (g/m^3) / A (atomic weight)
            nm = 6.02e23 * 13 * 2700e3 / 26.9815385; %(e/m^3)
        case {'17-4SS','SS','Stainless'}
            %model steel atomics as iron
            nm = 6.02e23 * 26 * 7800e3 / 55.845; %(e/m^3)
        case 'Brass'
            %model brass atomics as copper
            nm = 6.02e23 * 29 * 8490e3 / 63.546; %(e/m^3)
        otherwise
            nm = 6.02e23 * 13 * 2700e3 / 26.9815385; %(e/m^3)
    end
    
    %plasma frequency 
    wp = sqrt( nm * (1.602e-19)^2 / (9.11e-31 * 8.854187817e-12)); %rad/s
    
    %e(w) ~ 1 - wp^2 / w^2
    if w ~= 0
        e = 1 - wp^2 / w^2; %unitless freq dependant e
    elseif w == 0
        %for now assume 0th term has same permittivity as lube
        e = 0;
    else
        display('Error in Permittivity Equation');
    end
    
end

%% MAIN LOOP %%

display(sprintf('Calculating Please be Patient...'));
pause(.5);
display(sprintf('Calculating Casimir Force...'));
pause(.5);
%CASIMIR FORCE
%Casimir Pressure (SIMPLIFIED)
% P(l) ~ kT/4(pi)l^3 * sum'[delA*delB*(1 + rn + rn^2/2)e^-rn] :0 -> inf
%where sum' denotes 0th term is divided by 2
% P(l) = d(G)/dl (partial derivative)

%first term
r_n = rn(em,l,T,0);
zai = zai_n(T,0);
eA = e_Permittivity(metalA, zai); %permittivity of metals are frequency dependent
eB = e_Permittivity(metalB, zai);

Psum_l = (1/2) * del_bar(eA,em) * del_bar(eB,em) * ...
    (1 + r_n + r_n^2/2) * exp(-r_n);

display(sprintf('Calculating Eigen Frequency Values...'));
pause(.5);
%summation portion
for n = 1:nMAX
    r_n = rn(em,l,T,n);
    
    %permittivity of metals are frequency dependent
    zai = zai_n(T,n);
    eA = e_Permittivity(metalA, zai); 
    eB = e_Permittivity(metalB, zai);
    
    %pressure portion
    Psum_l = Psum_l + del_bar(eA,em) * del_bar(eB,em) * ...
        (1 + r_n + r_n^2/2) * exp(-r_n);
end

%front portion
Psum_l = Psum_l * -k * T / (4 * pi * l^3);

%P = F/A
F_cas = Psum_l*w*h; %Force of Casimir in N


%LUBRICANT NORMAL FORCE
display(sprintf('Calculating Lube Normal...'));
pause(.5);
F_lube_N = -2 * w * h * y_la * cos(theta_c) / g; %N
%force of lube pull due to surface tension of lube


%FORCE OF WEIGHT
display(sprintf('Calculating Weight Normal...'));
pause(.5);
F_w = - m * g; %Weight in N of the top block

%TOTAL NORMAL FORCE 
F = F_cas + F_lube_N + F_w;

%SHEAR CONTACT MODEL
if (contact_model)
    display(sprintf('Calculating Contact Model...'));
    pause(.5);
    %CONTACT MECHANICS (DMT MODEL OF ELASTIC CONTACT)
    %This portion is taken into account if the distance between the plates 
    %is small enough that the surface roughness peaks can be modelled as a 
    %set of breaking cantilever beams.  This threshold can be adjusted but
    %it is currently assumed to be less than the total RMS roughness of both
    %at the end lubrication shear will also be added
    
    %effective Young's modulus
    Es = E_star(EA,EB,vA,vB); %Pa

    %Probability Area Model
    % What i am attempting to do here is as follows:
    % -Model roughness as a collection of spheres
    % -Assume the radii of these spheres are normally distributed around the RMS
    % -Consider only the radii values of the std deviations from 3 - 6
    % -the probability mass under the space between std deviations represents the 
    %     amount of total area taken up by the radii of size x at the std deviation.
    %     so if std 3-3.5 is .0007m then assume 1.1e-5% of area is covered by radii .0007m
    % -By dividing total area by radius we can find the number of each sphere of radii x
    % -Assume these radii are the ones making contact under DMT theory.
    % -Assume the radii of equal sizes are matched up (for now)
    % -use dmt for two spheres to find contact radii R
    % -The force is also divided according to PDF, then by nA to find load
    %     P on the spheres.
    % -assume these are beams of circular cross section radii R
    % -use cantilever assumption to find individual breaking force.
    % -sum the forces
    % -apply log normal breaking coefficient
    % -take the minimum of the two values
    %portion under PDF Ax_xx is std deviation from x to x.x
    %Ap(3-3.5,3.5-4,4-4.5,4.5-5,5-5.5,5.5-6)

    %find the area of density function between half sigmas
    %this represents the portion of total area taken up by
    %radii in the particular sigma
    display(sprintf('Calculating Statistic Model...'));
    pause(.5);
    ApA = normcdf(s_R(s)+.25,h_pri_A,sigA) - normcdf(s_R(s)-.25,h_pri_A,sigA);
    ApB = normcdf(s_R(s)+.25,h_pri_B,sigB) - normcdf(s_R(s)-.25,h_pri_B,sigB);
    
    %find equivalent force applied to each hemisphere category
    Feff = ApA .* F;
    
    %portion of total area relative to pdf density then divide out pi so
    %result is just r^2 for each sigma
    ApA = ApA .* At ./ pi;  
    ApB = ApB .* At ./ pi;
    
    %divide the reduced area by ri^2 to get number of beams in that dev.
    nA = round(ApA ./ (s_RA(s)).^2); 
    nB = round(ApB ./ (s_RB(s)).^2);   
    
    %DMT contact radii
    display(sprintf('Calculating DMT Model...'));
    pause(.5);
    %mean contact of two "Spheres"
    % a^3 = 3R/4E*(F+4ypiR)
    % where a is area, R is effective radius, E* is effective modulus
    % y is surface energy (assume 0) 

    %effective radius of ith sigma
    %1/R* = 1/R1 + 1/R2
    Reff = R_star(s_RA(s), s_RB(s)); 
    
    %effective area of contact
    %force applied is the total force in the sigma region divided by
    %number of hemispheres in that catagory
    a = (3 * Reff .* ((abs(Feff)./nA) + 4*y*pi*Reff) ./ (4 * Es)).^(1/3);
    
    %effective cantilever radius
    R = sqrt(a ./ pi);

    %moments of inertia pi r^4 / 4
    IA = pi .* R.^4 ./ 4;
    IB = pi .* R.^4 ./ 4;    
    
    display(sprintf('Calculating Breaking Forces...'));
    pause(.5);
    %force to break 1 beam of size sigma
    %max_sig_fail = My/I, M = F * l -> F = sig * I / (l * y)
    FAi = SA .* IA ./ (l * R); 
    FBi = SB .* IB ./ (l * R);
    
    %quicker than pumping through two for loops.  Ir R == 0 then FAi goes
    %NaN.  Yea programming
    FAi(isnan(FAi)) = 0;
    FBi(isnan(FBi)) = 0;

    %total force to break all the beams of sigma
    FA = FAi .* nA .* C(s);
    FB = FBi .* nB .* C(s);
    
    %total force from considered sigmas
    FAo = sum(FA);
    FBo = sum(FB);

    %minimum to break
    F_contact = min(FAo,FBo);
else 
    F_contact = 0;
end

%SHEAR FLUID MODEL
if strcmp(lube_type,'liquid')
    if strcmp(vis_type,'bingham')
        %liquid shear force as shear stress times area. linear fit from
        %experimental temperature change average
        Tl = At * t0;  
    else
        %make space for the pillar model if needed
        Tl = 0;
    end
else
    Tl = 0;
end

%SHEAR SOLID MODEL
% this needs to be altered to a rolling/sliding friction concept
if strcmp(lube_type,'solid')
    display(sprintf('Calculating Solid Model...'));
    pause(.5);
    %establish angles 
    theta_g = 0:.01:pi/2;
 
    %ADD A DISTRABUTION TO ACCOUNT FOR HOW MANY OF EACH ANGLE
    %total number of contact grains 
    n_g = round(At / (pi * gs^2));
    
    display(sprintf('Calculating Number of Elements per Angle...'));
    pause(.5);
    x=1; %create index
    for i = -length(theta_g)/2:length(theta_g)/2-1
        %find the area under a normal curve using 0rad as -6sig and pi/2 
        %rad as 6sig hence near pi/4 is majority of prob mass 
        C_angle(x) = round((normcdf(i+1,45,9)-normcdf(i,45,9)) * n_g);
        x = x+1;
    end
    
    display(sprintf('Calculating Grain Normal Force...'));
    pause(.5);
    %split the total normal force evenly across each grain
    Fg = abs(F / n_g);
    %find the normal contact force between two grains 
    Fn = Fg * sin(theta_g);
    
    display(sprintf('Calculating DMT Grain Model...'));
    pause(.5);
    %find the contact radius using DMT theory
    a_g = real((3 * gs .* (Fn + 4*y_la*pi*gs) ./ (4 * Eg)).^(1/3));
    
    %contact pressure (assume constant)
    p0 = Fn / a_g;
    %t_i = 3; %just a marker for now
    
    display(sprintf('Calculating Shear...'));
    pause(.5);
    %until a shear value can be found, using friction coefficient of .2
    %find the individual shear force  
    %Tsi = Fn .* cos(theta_g) + .2 * Fn; %t_i .* a_g;
    
    %tsi  = opposing normal component + 12p0^5*R^3/(l^3*E^4)  from Goryacheva
    Tsi = Fn .* cos(theta_g) + 12 .* p0.^5 .* gs.^3 / ((2*gs)^3 .* Eg^4);
    %find the total shear force for each contact angle
    Ts_n = Tsi .* C_angle;
    
    %Sum each grain's contribution to total force
    Ts = sum(Ts_n);
else
    Ts = 0;
end

switch shear_model
    case 'Contact only'
        F_shear = F_contact;
    case 'Liquid with contact'      
        %liquied resistive shear 
        %combine the shear
        F_shear = F_contact + Tl;
    case 'Liquid Only'
        %liquid shear only
        F_shear = Tl;
    case 'Solid with contact'
        %solid with a few contact 
        F_shear = F_contact + Ts;
    case 'Solid only'    
        F_shear = Ts;
    otherwise 
        display('Your Shear Force Calculation is Broke');
end

mu = abs(F_shear/F); %non dimensionalize the static friction force

%% OUTPUTS %%

display(sprintf('\n'));
display(sprintf('All Done!\n'));
display(sprintf('The Casimir Force for this geometry and setup is: %dN',F_cas));
display(sprintf('The Lubricant Force for this geometry and setup is: %dN',F_lube_N));
display(sprintf('The Weight Force for this geometry and setup is: %dN',F_w));
display(sprintf('The Total Vertical Force for this geometry and setup is: %dN',F));
display(sprintf('The Contact Shear Force for this geometry and setup is: %dN', F_contact));
display(sprintf('The Lubricant Shear Force for this geometry and setup is: %dN', Tl));
display(sprintf('The Solid Shear Force for this geometry and setup is: %dN\n', Ts));
display(sprintf('The Total Shear Force for this geometry and setup is: ~%dN\n',F_shear));
display(sprintf(['The Non-dimensional Coefficient (F_shear/F) \n for this '...
    'geometry and setup is: %3.3f'], mu));
display(sprintf('\n')); %make a new line


end
